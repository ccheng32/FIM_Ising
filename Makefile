main: FIM_Ising.cpp
	g++  -Wall -fopenmp -lgomp -O3 -std=c++11 FIM_Ising.cpp -o main -I/home/ccheng32/local/include -L/home/ccheng32/local/lib64 -larmadillo
clean:
	rm -rf main *.o
